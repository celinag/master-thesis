\chapter{Sistema de Comunicações}\label{chap:communication-system}
	Neste capítulo mencionam-se as características das comunicações rádio e acústicas, a sua importância no cálculo de distâncias e as aproximações que podem ser feitas mediante determinadas condições. De seguida, apresentam-se duas propostas de troca de informação entre veículos e destes com o mergulhador, em que uma requer a sincronização de relógios e a outra não. Ainda se evidenciam alguns tipos de falhas que possam existir e, finalmente, apresentam-se as escolhas associadas ao projecto.\\
	
	\section{Análise de Sinais e Aproximações}
		As ondas rádio, também conhecidas por ondas electromagnéticas, propagam-se à velocidade da luz no vácuo. Estas ondas são usadas em radiodifusão, em comunicações via satélite, radioastronomia, radares e outros sistemas de navegação, redes de computadores e inúmeras outras aplicações.
		Por serem tão rápidas e fidedignas no ar, foram eleitas para a troca de mensagens entre veículos.\\
		
		As ondas acústicas podem percorrer longas distâncias na água ao contrário das ondas electromagnéticas, daí se ter optado por usar comunicações acústicas entre o mergulhador e os veículos. De facto, se as ondas electromagnéticas se propagassem na água da mesma forma que se propagam no ar, não seria necessário desenvolver sistemas de localização diferentes neste meio. Para mais informações consultar \citep{Taraldsen:2011fk}.
		
		No entanto, as comunicações acústicas têm alguns problemas como a reflexão do som, a dependência da salinidade e da profundidade, a sobreposição, o atraso, a distorção, a atenuação, entre outros. Na \autoref{fig: som_na_agua} \cite{http://www.punaridge.org/:1998zr} mostra-se a variação da velocidade em função da profundidade.
		
		\begin{figure}[!ht]
			\begin{center}
				\includegraphics[width=7cm]{Graphics/Comunicacoes/som_na_agua.jpg}
				\caption{Variação da velocidade do som na água do mar com a profundidade}
				\label{fig: som_na_agua}
			\end{center}
		\end{figure}
		
		A título de exemplo, considerando a velocidade média do som na água de 1450 metros por segundo, pode-se verificar que se existir um atraso de 10 milisegundos na medição do tempo de propagação de uma mensagem provocaria um erro de 14.5 metros na suposta distância percorrida pela mensagem.
		
		Para evitar o problema da sobreposição de mensagens podem-se usar dois tipos de separação de sinais: no tempo ou na frequência.
		
		Para as comunicações acústicas utiliza-se separação no tempo, como tal há que garantir que não são enviados sinais em simultâneo. Ou seja, quando uma entidade está a transmitir uma mensagem todas os outras têm que escutar ou aguardar.\\
		
		%\TODO{Dizer que: A correcta troca de mensagens foge ao âmbito deste projecto, qd se envia uma msg acústica ela pode ser ouvida por outras entidades além da destinatária, por outro lado cada interveniente pode receber mais sinais além dos previstos e deve saber quais sao para si descartando os restantes}
		
		Sem perda de generalidade, tome-se o caso em que as distâncias dos veículos ao mergulhador crescem por esta ordem: \(V_1\), \(V_2\), \ldots, \(V_n\) e seja \(V_1\) o veículo principal. Considere-se ainda a seguinte nomenclatura:
			\begin{description}
				\item[\(t_{ping}\)] instante de tempo em que o veículo principal envia uma mensagem para o mergulhador (\textit{Ping});
				\item[\(t_{D_{in}}\)] instante de tempo em que o mergulhador recebe uma mensagem do veículo principal (\textit{receives master's Ping message});
				\item[\(t_{D_{out}}\)] instante em que o mergulhador transmite uma mensagem em \textit{broadcast} (\textit{Pong});
				\item[\(t_{V_i}\)] instante em que o veículo \(i\) recebe a mensagem do mergulhador (\textit{receives diver's Pong message});
				\item[\(t_{V_1} \equiv t_{M}\)] instante em que o veículo principal recebe a mensagem do mergulhador (\textit{receives diver's Pong message});
				\item[\(d_{DV_i}\)] distância entre o mergulhador e o veículo \(i\);
				\item[\(d_{DM}\)] distância entre o mergulhador e o veículo principal;
				\item[\(T_{Dp}\)] tempo de processamento do mergulhador;
				\item[\(\Delta t_{V_i}\)] intervalo de tempo a partir da transmissão da mensagem pelo mergulhador (\textit{Pong}) até que o veículo \(i\) a recebe;
				\item[\(\Delta t_{M}\)] intervalo de tempo a partir da transmissão da mensagem pelo mergulhador (\textit{Pong}) até que o veículo principal a recebe (é o mesmo intervalo de tempo que decorre desde a transmissão da mensagem do veículo principal (\textit{Ping}) até que o  mergulhador a recebe);
				\item[\(\Delta t_{MV_i}\)] intervalo de tempo desde a transmissão da mensagem pelo veículo principal (\textit{Ping}) até que o veículo \(i\) a recebe;
			\end{description}
			
		Esta nomenclatura pode ser mais facilmente entendida analisando a \autoref{fig: communications_in_time}, onde veículos e mergulhador estão parados durante a troca de mensagens.
		
		\begin{figure}[!ht]
			\begin{center}
				\includegraphics[width=14cm]{Graphics/Comunicacoes/time-propagation.pdf}
				\caption{Comunicações acústicas no tempo}
				\label{fig: communications_in_time}
			\end{center}
		\end{figure}
		
		No entanto, para não haver limitações no sistema, os veículos e o mergulhador podem estar parados ou em movimento, aliás mesmo que um veículo esteja ancorado, a sua posição pode-se alterar devido ao movimento da água.
		
		Ainda assim, assume-se que durante a troca de mensagens os intervenientes estão parados, devido à deslocação dos veículos e do mergulhador durante um ciclo de troca de mensagens ser muito menor do que o seu próprio tamanho.
		Vejamos um exemplo com valores típicos, de forma a perceber que tipo de aproximação é feita, com o apoio da \autoref{fig: time-propagation-movement}.
		
		\begin{figure}[!ht]
			\begin{center}
				\includegraphics[scale=0.7]{Graphics/Comunicacoes/time-propagation-movement.pdf}
				\caption{Distâncias durante um ciclo de troca de mensagens}
				\label{fig: time-propagation-movement}
			\end{center}
		\end{figure}
		
		Usa-se o modelo mais simples de propagação do som na água: \(\ d =  \Delta t \, v_{s} \), isto é, a distância percorrida por um sinal acústico é dada pelo produto entre o intervalo de tempo que este demorou a percorrer essa distância e a velocidade do som na água, aqui assumida como constante.
		
		Considere-se que o veículo principal envia o \textit{ping} e a onda percorre a distância de $\unit{50}{\meter}$ até chegar ao mergulhador, demorando desta forma:
		\begin{equation}\nonumber
			t_{D_{in}} - t_{ping}= \frac{d_{DM}}{v_{som}} = \frac{50}{1450} \simeq 0.0345 \unit{}{\second}
		\end{equation}
		
		Neste intervalo de tempo o veículo percorreu a seguinte distância 
		
		\begin{equation}\nonumber
			d_1 = \frac{d_{DM}}{v_{som}} \cdot v_{veiculo} = \frac{50}{1450} \cdot 0.8 = \unit{0.0276}{\meter} = \unit{27.6}{\milli\meter}
		\end{equation}
		
		Durante o tempo de processamento da \textit{interface} do mergulhador, este move-se cerca de 
		
		\begin{equation}\nonumber
			d_4 = T_{Dp} \cdot v_{mergulhador} = 0.1 \cdot 0.2 = \unit{0.02}{\meter} = \unit{20}{\milli\meter}
		\end{equation}
		
		Durante esse mesmo tempo, o veículo percorre 
		
		\begin{equation}\nonumber
			d_2 = T_{Dp} \cdot v_{veiculo} = 0.1 \cdot 0.8 = \unit{0.08}{\meter} = \unit{80}{\milli\meter}
		\end{equation}
		
		Por simplificação, assume-se que a distância que o veículo percorre desde que o \textit{pong} é emitido até ser recebido é igual ao percurso inverso percorrido pelo \textit{ping}, isto é,
		\begin{equation}\label{eq:d3-d1}
			d_3 = d_1 = \unit{27.6}{\milli\meter}
		\end{equation}
		
		Então, entre o envio de \textit{ping} e a recepção de \textit{pong} o veículo deslocou-se:
		\begin{equation}\nonumber
			d_1 + d_2 + d_3 = 2\cdot d_1+ d_2 = 2\cdot 0.0276 + 0.08 = \unit{0.1352}{\meter} = \unit{135.2}{\milli\meter}
		\end{equation}
		
		e o mergulhador deslocou-se:
		\begin{equation}\nonumber
			d_4 = \unit{20}{\milli\meter}
		\end{equation}
		
		A tendência será para que os veículos sigam o percurso do mergulhador, mas no pior cenário considera-se que este veículo se afasta, como demonstrado em \autoref{fig:time-propagation-distance} que geometricamente é equivalente a \autoref{fig:time-propagation-distance-triangle}. 
		
		\begin{figure}[!ht]
				\begin{center}
					\subfloat[Distâncias máximas]{\label{fig:time-propagation-distance}\includegraphics[scale=0.5]{Graphics/Comunicacoes/time-propagation-distance.pdf}}
					\subfloat[Equivalente geométrico]{\label{fig:time-propagation-distance-triangle}\includegraphics[scale=0.5]{Graphics/Comunicacoes/time-propagation-distance-triangle.pdf}}
					\caption{Pior cenário de movimento de veículo e mergulhador}
					\label{fig:msg}
				\end{center}
		\end{figure}
		
		A distância $d_{DM2}$ pode ser calculada aplicando o teorema de Pitágoras:
		
		\begin{equation}\nonumber
			d_{DM2} = \sqrt{(d_1 + d_2 + d_3 + d_4)^2 + d_{DM1}^2} = \sqrt{0.1552^2 + 50^2} = \unit{50.0002}{\meter}
		\end{equation}
		
		A diferença entre as distâncias percorridas pelas ondas é da tal forma pequena, que a aproximação feita em \eqref{eq:d3-d1} é válida e ainda se pode assumir que:
		\begin{equation}\nonumber
			d_{DM2} = d_{DM1}
		\end{equation}
		
		Durante este ciclo de troca de mensagens, o veículo e o mergulhador tiveram deslocações da ordem dos milimetros, claramente desprezáveis face aos seus tamanhos. É, portanto, plausível assumir que o mergulhador e os veículos estão parados desde o envio do \textit{ping} à recepção do \textit{pong}.\\
		
		Existem duas formas para definir a estratégia de troca de mensagens: uma requer sincronismo de relógios e a outra não.
		
		Sem relógios síncronos poupa-se o tempo de sincronização e em vez de se enviarem instantes de tempo, enviam-se intervalos de tempo. Estes intervalos são calculados nos veículos desde o aviso do veículo principal até à recepção da resposta do mergulhador, o que torna necessário que este aviso seja enviado via rádio a todos os veículos por parte do veículo principal.
		
		Com relógios síncronos é necessário ter especial atenção à precisão dos relógios e do sistema de sincronização, pois estes poderão introduzir erros nas distâncias calculadas. No entanto, as mensagens são mais curtas e o tempo de transmissão é menor.
	
	\section{Estratégia de Troca de Mensagens Sem Relógios Síncronos}\label{sec:sem-relogios-sincronos}
		
		A abordagem adoptada que não requer sincronismo de relógios é a seguinte:
		\begin{enumerate}
			\item \label{msg-chain-ping-alert}
				O veículo principal avisa os restantes veículos (usando um sinal de rádio) que vai enviar um \textit{ping} para o mergulhador - \textit{Ping alert};
			\item \label{msg-chain-ping}
				Ao mesmo tempo, o veículo principal envia o \textit{ping} para o mergulhador (usando um sinal acústico) - \textit{Ping};
			\item
				O mergulhador processa o sinal recebido e transmite uma mensagem em \textit{broadcast}\\ (para todos os veículos, incluindo o principal) - \textit{Pong};
			\item \label{msg-chain-pong-from-diver}
				Todos os veículos processam o sinal enviado pelo mergulhador - \textit{Pong from the diver};
			\item
				Os veículos simples enviam, via rádio, a informação processada no \autoref{msg-chain-pong-from-diver} para o veículo principal;
			\item \label{msg-chain-diver-pos}
				O veículo principal calcula a posição estimada do mergulhador usando as informações processadas no \autoref{msg-chain-pong-from-diver} de todos os veículos incluindo a dele próprio - \textit{Calculate diver's position};
			\item
		Repetem-se os Itens de \ref{msg-chain-ping-alert} a \ref{msg-chain-diver-pos} e no \autoref{msg-chain-ping} o veículo principal passa a enviar, na mensagem, a direcção que o mergulhador deve tomar (baseada nos cálculos efectuados no \autoref{msg-chain-diver-pos}).
		\end{enumerate}
		
		A \autoref{fig:comm-without-ntp} apresenta um exemplo da troca de mensagens rádio e acústicas, na ausência de erros de comunicação. Como pode ser visto, o sinal rádio é praticamente instantâneo, uma vez que a onda se desloca à velocidade da luz, ao contrário das ondas acústicas cuja propagação é bastante mais lenta.\\
		
		\begin{figure}[!ht]
			\begin{center}
				\subfloat[Rádio]{\label{fig:comm-without-ntp-radio}\includegraphics[scale=0.8]{Graphics/Comunicacoes/comm-without-ntp-radio}}\\
				\subfloat[Acústicas]{\label{fig:comm-without-ntp-acustic}\includegraphics[scale=0.8]{Graphics/Comunicacoes/comm-without-ntp-acustic}}
				\caption{Troca de mensagens sem relógios síncronos}
				\label{fig:comm-without-ntp}
			\end{center}
		\end{figure}
		
		De seguida, analisa-se de que forma esta troca de mensagens contribui para determinar as distâncias entre o mergulhador e os veículos.\\
		
		Após o primeiro ciclo de troca de mensagens, o veículo principal tem as seguintes informações: \(t_{ping}\), \(t_{M}\) e \(\Delta t_{MV_i}\) para todos os veículos.
		
		Com esta informação, é possível determinar as distâncias entre o mergulhador e todos os veículos.\\
		
		Começa-se por calcular a distância entre o mergulhador e o veículo principal, \( d_{DM} \):
		
		\begin{equation}\label{eq:distance_between_diver_maste_assincronosr}
			d_{DM} = \Delta t_{M}  \, v_{s},
		\end{equation}
		
		onde
		
		\begin{equation}\label{eq:delta_t_envio_diver_recepcao_veiculo}
			\Delta t_{M} = \frac{t_{M} - t_{ping} - T_{Dp}}{2}
		\end{equation}
		
		Analogamente, a distância entre o mergulhador e o veículo \(i\), \( d_{DV_i} \), é calculada usando
		
		\begin{equation}\label{eq:distance_between_diver_vehicle}
			d_{DV_i} = \Delta t_{V_i}  \, v_{s},
		\end{equation}
		
		onde
		
		\begin{equation}\label{eq:distances}
			\Delta t_{V_i} = \Delta t_{MV_i} - \Delta t_{M} - T_{Dp}
		\end{equation}
		
		No fim deste processo obtém-se, desta forma, todas as distâncias do mergulhador a cada um dos veículos.\\
		
	\section{Estratégia de Troca de Mensagens Com Relógios Síncronos}\label{sec:com_relogios_sincronos}
		A abordagem que requer a sincronização dos relógios é semelhante à que se acabou de descrever, com a diferença da sincronização inicial de relógios e do conteúdo das mensagens.\\
		
		Para sincronizar os relógios pode-se usar o protocolo NTP (Network Time Protocol) que é baseado no UDP e uma fonte fiável de tempo (normalmente servidores de NTP públicos). Este protocolo permite sincronizar os relógios com grande precisão. NTPv4 consegue manter a precisão dentro de 10 milisegundos (1/100 s) usando um servidor público e uma precisão de 200 microsegundos (1/5000 s) ou melhor em redes de área locais (LANs) dentro de condições ideais. Poderíamos optar pelo protocolo SNTP (versão mais simples do NTP), mas este não garante tanta precisão como a que necessitamos para as comunicações deste sistema. \citep{Fan:2011kx}.
		
		Apesar de não ser o focos desta tese, é importante que a sincronização dos relógios seja inferior a 10 milissegundos (para que o erro na distância inserido pelos relógios seja inferior a 14.5 centímetros), para mais informação sobre este assunto consultar \citep{Ruiqing:2010fk} e \citep{He:2009fk}.\\
		
		Depois de sincronizados os relógios dos veículos, o procedimento é muito semelhante ao definido na \autoref{sec:sem-relogios-sincronos}. A troca de mensagens mantém-se, mas sem o alerta de \textit{ping}, \autoref{msg-chain-ping-alert}, tal como pode ser visto na \autoref{fig:msg_com_relogio_sincrono}. A \autoref{fig: communications_in_time} mantém-se válida, apresentando as comunicações acústicas em função do tempo.
		
		\begin{figure}[!ht]
			\begin{center}
				\subfloat[Rádio]{\label{fig:comm-with-ntp-radio}\includegraphics[scale=0.8]{Graphics/Comunicacoes/comm-with-ntp-radio}}\\
				\subfloat[Acústicas]{\label{fig:}\includegraphics[scale=0.8]{Graphics/Comunicacoes/comm-with-ntp-acustic}}
				\caption{Troca de mensagens com relógios síncronos}
				\label{fig:msg_com_relogio_sincrono}
			\end{center}
		\end{figure}
		
		Após o primeiro ciclo de troca de mensagens o veículo principal tem a seguinte informação: \(t_{ping}\) e \(t_{V_i}\) para todos os veículos incluindo ele próprio, \(t_{M}\).
		
		Para determinar as distâncias é necessário efectuar os seguintes cálculos:
		
		\begin{equation}\label{eq:distance_between_diver_master_sincronos}
			d_{DM} = \Delta t_{M} \, v_{s},
		\end{equation}
		
		onde
		
		\begin{equation}\label{eq:delta_t_envio_diver_recepcao_veiculo}
			\Delta t_{M} = \frac{t_{M} - t_{ping} - T_{Dp}}{2}
		\end{equation}
		
		Da mesma forma, \( d_{DV_i} \) é calculado usando:
		
		\begin{equation}\label{eq:distance_between_diver_vehicle}
			d_{DV_i} = \Delta t_{V_i} \, v_{s},
		\end{equation}
		
		onde
		
		\begin{equation}\label{eq:distances}
			\Delta t_{V_i} = t_{V_i} - t_{ping} - \Delta t_{M} - T_{Dp}
		\end{equation}
		
		
	\section{Falhas de Comunicações}\label{sec:falhas_comunicacao}
		Dadas as características das ondas rádio, a troca de mensagens entre veículos assume-se robusta a falhas. Já as acústicas, ao operam dentro de água, são mais sensíveis a falhas de comunicação, levando ao atraso de mensagens ou mesmo à sua perda.\\
		
		Para que os algoritmos de localização funcionem é necessário garantir a sequência correcta da troca de mensagens, ignorar mensagens atrasadas pertencentes a ciclos anteriores e detectar perdas de mensagens (\textit{timeout}).
		
		A \autoref{fig:msg_perdas_geral} apresenta 3 tipos de falhas de mensagens. No diagrama temporal \autoref{fig:comm-fail-diver}, o sinal - \textit{ping} - que o veículo principal enviou não chegou ao mergulhador e este não emitirá qualquer sinal de resposta, consequentemente nada chegará aos veículos.
		
		No diagrama \autoref{fig:comm-fail-master}, o mergulhador recebe o \textit{ping} e responde em \textit{broadcast}, no entanto o veículo principal não chega a receber este sinal.
		
		Por fim, no diagrama \autoref{fig:comm-fail-vehicle}, ocorre uma falha na recepção do \textit{pong} num dos veículos.
		
		Todas as falhas referidas são detectadas no veículo principal usando um marcador de \textit{timeout} que é accionado sempre que é enviado um sinal \textit{ping}. Quando o tempo de \textit{timeout} é atingido, inicia-se outro ciclo de troca de mensagens e todas as mensagens dos ciclos anteriores são ignoradas.
		
		\begin{figure}[!ht]
			\begin{center}
				\subfloat[Mergulhador]{\label{fig:comm-fail-diver}\includegraphics[width=10cm]{Graphics/Comunicacoes/comm-fail-diver}}\\
				\subfloat[Veículo Principal]{\label{fig:comm-fail-master}\includegraphics[width=10cm]{Graphics/Comunicacoes/comm-fail-master}}\\
				\subfloat[Veículo Comum]{\label{fig:comm-fail-vehicle}\includegraphics[width=10cm]{Graphics/Comunicacoes/comm-fail-vehicle}}
				\caption{Falhas nas comunicações acústicas}
				\label{fig:msg_perdas_geral}
			\end{center}
		\end{figure}
		
		Na \autoref{fig:comm-fail-delay} apresenta-se como o sistema se comporta caso exista atraso na chegada de uma mensagem. O veículo principal enviou um sinal - \textit{ping 1} - e, por não obter resposta do mergulhador, iniciou outro ciclo de troca de mensagens, enviando o sinal \textit{ping 2}. Quando é esperado o \textit{pong 2} na verdade o veículo recebe o \textit{pong 1}. Neste caso, o \textit{pong 1} é identificado e ignorado pelo veículo principal, se não fosse, o sistema iria assumir que o mergulhador estaria mais próximo do que está na realidade.
		
		\begin{figure}[!ht]
			\begin{center}
				\includegraphics[width=8cm]{Graphics/Comunicacoes/comm-fail-delay}
				\caption{Atraso nas comunicações acústicas}
				\label{fig:comm-fail-delay}
			\end{center}
		\end{figure}
		
	\section{Comunicações Aplicadas ao Projecto}
		Das duas opções de implementação definidas neste capítulo, optou-se por trabalhar com relógios síncronos, uma vez que esta sincronização é assegurada pelo equipamento GPS presente nos veículos.\\
		
		Os cálculos das distâncias a partir da informação recolhida na troca de mensagens são feitos de acordo com as equações \eqref{eq:distance_between_diver_master_sincronos}, \eqref{eq:delta_t_envio_diver_recepcao_veiculo}, \eqref{eq:distance_between_diver_vehicle} e \eqref{eq:distances} definidas na \autoref{sec:com_relogios_sincronos}.\\
		
		Com as distâncias entre o mergulhador e os veículos obtidas de acordo com o que foi descrito neste capítulo e as posições geográficas dadas pelo GPS, consegue-se calcular a posição geográfica do mergulhador em cada ciclo.
			