\chapter{Trilateração}\label{chap:trilateracao}
	
	Este capítulo é dedicado à trilateração, contém uma explicação intuitiva do seu algoritmo, as equações matemáticas correspondentes a uma opção de implementação e uma proposta de aplicação à localização de um mergulhador.\\
	
	A trilateração tem como objectivo determinar a localização absoluta ou relativa de pontos com base em distâncias, utilizando a geometria de esferas e triângulos. Estas distâncias entre o objecto a localizar e cada ponto de referência devem ser medidas em simultâneo.
	
	Ao contrário da triangulação, que utiliza medidas de ângulo, a trilateração baseia-se apenas em distâncias.
	
	A trilateração é usada em aparelhos de GPS, na localização de dispositivos de comunicações móveis (telemóveis), em cartografia (no levantamento planimétrico), etc..\\
	
	O seu princípio intuitivo é o seguinte: imaginemos que estamos num lugar desconhecido, temos um mapa e a indicação da nossa distância a 3 cidades. Recorrendo à trilateração, marca-se no mapa uma circunferência com centro na cidade A e raio igual à nossa distância à mesma, repete-se para a cidade B e a intersecção das duas circunferências origina dois ponto onde poderemos estar, finalmente, repetindo para a terceira cidade ficamos com um único ponto e já sabemos onde estamos. São, portanto, necessários 3 pontos de referência para uma localização a 2D.
	
	Para determinar a localização de um ponto no espaço tridimensional utilizando a trilateração são necessários pelo menos quatro pontos de referência. Ou seja, aplicando ao caso em estudo, são necessários quatro veículos para determinar de forma inequívoca a posição do mergulhador.
	
	Em torno de cada veículo imagina-se uma superfície esférica que representa a incerteza da posição do mergulhador, com raio igual à distância mergulhador - veículo. A posição estimada do mergulhador é obtida pela intersecção das esferas correspondentes a cada veículo. A intersecção de duas esferas gera um círculo, que quando intersectado com uma terceira esfera gera dois pontos onde o mergulhador pode estar. Finalmente, a desambiguação é feita com a ajuda de uma quarta esfera, deixando apenas um único ponto.
	
	Na verdade, a trilateração requer apenas 3 veículos, porque um dos dois pontos obtidos na intersecção da terceira esfera é totalmente inadequado dada a posição anterior do mergulhador, podendo até estar fora de água (no caso do GPS, um dos pontos pode estar fora da superfície terrestre).
	
	Uma vez que as distâncias calculadas não são precisas, quantos mais veículos existirem, melhor será a estimativa da posição do mergulhador. A falta de precisão das distâncias introduz o problema de existirem duas ou mais posições possíveis para o mergulhador, que no pior caso será um volume de incerteza. É, portanto, necessário escolher a melhor estimativa.\\
	
	Existem várias formas de implementar a trilateração sendo uma delas o algoritmo do ponto fixo descrito em \citep{Penas:2009uq}.
	
	O algoritmo do ponto fixo, ao contrário de alguns algoritmos com filtros, usa apenas as medidas adquiridas no próprio instante de tempo para calcular a posição do objecto, ignorando a sua dinâmica e as medidas obtidas no passado. Por ser simples, pode-se implementar em plataformas com pouco poder computacional e pouca bateria, como os microcontroladores.
	
	Uma vez que não depende de informação passada, mas apenas das medidas actuais, o algoritmo do ponto fixo não propaga o erro, sendo assim mais robusto a valores atípicos e falhas nos sensores, umas vez que estas só afectam a estimativa em causa e não as futuras.
	
	No entanto, o desempenho e a estimativa dos erros deste tipo de algoritmo é pior do que os baseados em filtros mais sofisticados e algoritmos dinâmicos.
	
	Uma forma de resolver este problema do ponto fixo é usar o Método dos Mínimos Quadrados, LMS, que procura encontrar o melhor ajustamento para um conjunto de dados tentando minimizar a soma dos quadrados das diferenças entre a curva ajustada e os dados. Esta técnica de optimização é habitualmente usada na parametrização de curvas, mas também é aplicada a problemas de minimização ou maximização. No domínio estatístico, os requisitos que garantem a convergência do algoritmo são os seguintes:
	\begin{itemize}
		\item os erros em cada medida devem ser independentes e ter uma função densidade de probabilidade Gaussiana;
		\item o modelo deve ser linear, ou seja, as variáveis devem apresentar uma relação linear entre si.
	\end{itemize}
	Como ambos os requisitos são respeitados no projecto em estudo, este algoritmo pode ser aplicado.
	
	De seguida, apresentam-se as equações usadas na implementação do Método dos Mínimos Quadrados como aplicação da técnica de trilateração.\\
	
	Tome-se \(n\) veículos (\(n\geqslant 4\)) e seja \(p_ {D} \in \Re ^ 3 \) o vector com as coordenadas do centro de massa do mergulhador
e \(P_{_{V}}\) a matriz que contém as 3 coordenadas dos \(n\) veículos, conhecidas \textit{a priori} ou obtidas em tempo real a partir de GPS, isto é,
	
	\begin{equation}
		P_{_{V}} =
		\begin{bmatrix}
			p_{_{V_1,1}}	&	p_{_{V_1,2}}	&	p_{_{V_1,3}} \\
			\vdots & \vdots & \vdots \\
			p_{_{V_n,1}}	&	p_{_{V_n,2}}	&	p_{_{V_n,3}}
		\end{bmatrix}
	\end{equation}

A distância entre o mergulhador e o veículo \(i\) é adquirida de acordo com as equações \eqref{eq:distance_between_diver_master_sincronos} e \eqref{eq:distance_between_diver_vehicle} e agrupada com as restantes distâncias no vector \(d\)
	\begin{equation}\label{eq: distancia}
		d = 
		\begin{bmatrix}
			d_{DM} & d_{DV_2} & \cdots & d_{DV_i}
		\end{bmatrix}
		^T
	\end{equation}
	
	Cada uma destas distâncias corresponde a:
	\begin{align}\label{eq: distancia_calculo}
		d_{DV_i}^2 & = \|p_{D} - p_{V_i}\|^2\\ \nonumber
		& = (p_{D} - p_{V_i})^T (p_{D} - p_{V_i})\\ \nonumber
		& = p_{D}^T p_{D} - p_{D}^Tp_{V_i} - p_{V_i}^T p_{D} + p_{V_i}^T p_{V_i}\\ \nonumber
		& = p_{D}^T p_{D} - 2p_{V_i}^T p_{D} + p_{V_i}^T p_{V_i}
	\end{align}
	
	Define-se $\mathbb 1 \in \Re^n$ como um vector com o valor $1$ em todas as posições e $\Lambda$ como um operador que converte uma matriz quadrada num vector com a sua diagonal e vice-versa.
	
	Juntando as equações \eqref{eq: distancia} e \eqref{eq: distancia_calculo} obtém-se a mesma informação, na forma compacta:
	
	\begin{equation}\label{eq: distancia_compacta}
		d^2 = \|p_{D}\|^2{\mathbb 1}_n - 2P_{V} p_{D} + \Lambda (P_{V}^T P_{V})
	\end{equation}
	
	Esta equação é linear excepto para o termo $\|p_{D}\|^2{\mathbb 1}_m$. Com o objectivo de retirar este termo da equação, define-se a matriz de projecção $M \in \Re ^ {n \times n}$,
	\begin{equation}\label{eq: matriz_M}
		M = I_n - \frac{1}{n}{\mathbb 1}_n {\mathbb 1}_n ^T = \frac{1}{n} 
		\left(
			\begin{array}{ccccc}
				n-1 & -1 & & ... & -1 \\
				-1 & n-1 & & & \vdots \\
				\vdots & & &  \ddots & 0 \\
				-1 & \cdots & & -1 & n-1
			\end{array}
		\right)
	\end{equation}
	
	Esta matriz tem as seguintes propriedades:
	\begin{align} \nonumber
		M{\mathbb 1}_n &= 0\\ \nonumber
		M &= M^T\\ \nonumber
		MM &= M \nonumber
	\end{align}
	
	Multiplicando ambos os lados da \autoref{eq: distancia_compacta} por $M$, \citep{MatrixAnalysis}, obtém-se:
	\begin{align}
		Md^2 	&= M\|p_{D}\|^2{\mathbb 1}_n - 2MP_{V}^T p_{D} + M\Lambda (P_{V}^T P_{V})\\ \nonumber
				&= 0 - 2MP_{V}^T p_{D} + M\Lambda (P_{V}^T P_{V})\\ \nonumber
	\end{align}
	
	Logo,
	\begin{align}
		2MP_{V}^T p_{D} 	&= M\Lambda (P_{V}^T P_{V})-Md^2\\ \nonumber
						&= M[\Lambda (P_{V}^T P_{V})-d^2]
	\end{align}
	que é um sistema linear da forma $Ax = b$.
	
	Em que condições este sistema tem uma única solução? Será possível a partir destas condições fazer uma interpretação geométrica sobre o número de pontos de referência e a sua geometria?
	
	Como descrito em \citep{Penas:2009uq} é apresentada uma forma de resolver este sistema que se resume ao seguinte conjunto de equações:\\
	
	\begin{equation}\label{eq: trilateration}
		\left\{
		\begin{aligned}
			\Psi &= \frac{1}{n} \cdot P_{_{V}}^T \cdot \mathbb 1_n \\
			\Gamma &= P_{_{V}}^T- \Psi \cdot \mathbb 1_n ^T \\
			p_{m} &= \frac{1}{2}(\Gamma \cdot \Gamma^T)^{-1} \Gamma [\Lambda(P_{_{V}} \cdot P_{_{V}}^T) - d]
		\end{aligned}
		\right.
	\end{equation}\\
	onde:
	\begin{itemize}
		\item $p_{m}$ contém a posição medida do mergulhador;
		\item $\Psi \in \Re^{3}$ é um vector que contém as coordenadas do centróide dos pontos de referência (veículos).
	\end{itemize}
	
	A matriz $\Gamma  \cdot \Gamma^T$ é invertível se e só se $\Gamma \in \Re^{n \times n}$ tiver todas as colunas linearmente independentes.
	
	O problema de trilateração resolvido pelo LMS tem uma única solução se e só se existir pelo menos um conjunto de $n + 1$ pontos de referência que não se encontrem num subespaço afim de $R^n$. Ou seja, no caso bidimensional, para $n = 2$, são necessários pelo menos 3 pontos de referência não colineares. No espaço 3D, $n = 3$, são necessários pelo menos 4 pontos de referência não coplanares \citep{Thomas:2005vn}.\\
	
	Assim, além de serem necessários 4 veículos, é crucial garantir que não pertencem todos ao mesmo plano. Uma vez que todos os veículos estão à superfície do mar com os hidrofones submersos, é necessário descer o hidrofone de um deles. Devido ao constante movimento provocado pela ondulação, o ideal será colocar os hidrofones a diferentes profundidades.\\
	
	Aplicando a trilateração obtém-se a primeira estimativa da posição do mergulhador. No entanto, não se teve em conta a existência de erros ou ruído, o que não é verosímil dadas as características do mar e a sua influência sobre as comunicações acústicas.
	
	Outro ponto importante, que não faz parte do âmbito deste documento, é o estudo da posição dos veículos que optimiza o resultado da trilateração.\\
	
	Qual será o desempenho deste algoritmo apesar das aproximações efectuadas?
	Será que o número de veículos influência em muito os resultados?
	O que poderá ser feito para melhorar o algoritmo?
	
	%\TODO{Produzir um gráfico com esferas e intersecção, se possível e colocar lá em cima}