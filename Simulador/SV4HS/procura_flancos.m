function [xt, y, resto] = procura_flancos(valores, sinal)

resto = 0;
xt = []; y = [];
    for i = 1: size(valores.signals.values, 1)
        if valores.signals.values(i) == 0
            xt = cat(1, xt,valores.time(i));
            y = cat(1, y, 0);
        else
            xt = cat(1, xt, valores.time(i));
            y = cat(1, y, sinal);
            resto = cat(1, resto, valores.signals.values(i));
        end
    end