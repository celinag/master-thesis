% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Celina Gaiao  %  Instituto Superiror Técnico    %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc; clear all; close all;



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Inicializa dados
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('inicializa_vD_vV_falhas');



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        SIMULA
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sim('B_Vi_D.mdl', 600);


PT = 1;
EN = 0;


 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %        Trata dados pt
 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if PT
    % dist total
    DEBUG = 2; run('trata_dados');
    % print -depsc vD_vV_falhas_distancias_zoom
    % print -depsc vD_vV_falhas_distancias


    % grafico 3D
    DEBUG = 3; run('trata_dados');
    % print -depsc vD_vV_falhas_trajectoria_3D

    % trajectória do Diver segundo x y z sem pontos a zeros
    DEBUG  = 34; run('trata_dados');
    % print -depsc vD_vV_falhas_trajectoria_x
    % print -depsc vD_vV_falhas_trajectoria_y
    % print -depsc vD_vV_falhas_trajectoria_z
    % print -depsc vD_vV_falhas_trajectoria_x_zoom
    % print -depsc vD_vV_falhas_trajectoria_y_zoom
    % print -depsc vD_vV_falhas_trajectoria_z_zoom

    % erro Kalman + trilateração   
    DEBUG = 35; run('trata_dados');
    % print -depsc vD_vV_falhas_erro_posicao
    % print -depsc vD_vV_falhas_erro_velocidade
    % print -depsc vD_vV_falhas_erro_covariancia
    % print -depsc vD_vV_falhas_erro_covariancia_zoom

    % instantes de resposta dos veiculos
    DEBUG = 41; run('trata_dados');      
    % print -depsc vD_vV_falhas_tempos_boia_diver_zoom_bom
    % print -depsc vD_vV_falhas_tempos_veiculos_zoom_bom
    % print -depsc vD_vV_falhas_tempos_boia_diver_zoom_mau
    % print -depsc vD_vV_falhas_tempos_veiculos_zoom_mau


    % Falhas
    DEBUG = 6; run('trata_dados');
    % print -depsc vD_vV_falhas_aleatorias_zoom
    % print -depsc vD_vV_falhas_threshold
end



 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %        Trata dados en
 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if EN
    % dist total
    DEBUG = 2; run('trata_dados_en');
    % print -depsc vD_vV_falhas_distancias_zoom
    % print -depsc vD_vV_miss_distance


    % grafico 3D
    DEBUG = 3; run('trata_dados_en');
    % print -depsc vD_vV_miss_3D

    % trajectória do Diver segundo x y z sem pontos a zeros
    DEBUG  = 34; run('trata_dados_en');
    % print -depsc vD_vV_miss_path_x
    % print -depsc vD_vV_falhas_trajectoria_y
    % print -depsc vD_vV_falhas_trajectoria_z
    % print -depsc vD_vV_falhas_trajectoria_x_zoom
    % print -depsc vD_vV_falhas_trajectoria_y_zoom
    % print -depsc vD_vV_falhas_trajectoria_z_zoom

    % erro Kalman + trilateração   
    DEBUG = 35; run('trata_dados_en');
    % print -depsc vD_vV_miss_position_error
    % print -depsc vD_vV_falhas_erro_velocidade
    % print -depsc vD_vV_miss_cov_error
    % print -depsc vD_vV_falhas_erro_covariancia_zoom

    % instantes de resposta dos veiculos
    DEBUG = 41; run('trata_dados_en');      
    % print -depsc vD_vV_miss_time_master_diver_zoom
    % print -depsc vD_vV_miss_time_vehicle_zoom
    % print -depsc vD_vV_miss_time_vehicle_compare_cov


    % Falhas
    DEBUG = 6; run('trata_dados_en');
    % print -depsc vD_vV_falhas_aleatorias_zoom
    % print -depsc vD_vV_falhas_threshold
end

%    InsetZoomPlot
