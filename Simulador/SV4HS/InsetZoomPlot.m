
function handles = InsetZoomPlot(h)
% handles = InsetZoomPlot(h)
%
% This function draws a zoom of a plot in a new axes inside the plot
%
% First select a part of the plot by dragging the mouse. Second draw a box
% in the plot where the inset is going to be placed. The box drawing is
% done using imrect which is part of the image processing toolbox.
%
% This function draws a patch rectangle around the first selection, this
% patch can be removed using:
% >> delete(handles.patch)
% or made invisable using:
% >> set(handles.patch,'Visible','off')
%
% the output if this function is a structure containing the handles of the
% used objects:
% handles.main      = main axes
% handles.mainplot  = the plot in the main axes
% handles.inset     = the inset axes
% handles.insetplot = the plot in the inset axes
% handles.patch     = the patch
%
% tip: to modify the label of the inset axes:
% set(get(handles.inset,'XLabel'),'String','SomeLabel')

% Created by: Jan Neggers, Eindhoven, July 15th 2009

% testing if a handle is given
if nargin < 1
    h = gca;
end
% initializing the output structure
handles.main = h;
handles.mainplot = findobj(h,'-property','XData');
handles.inset = [];
handles.insetplot = [];
handles.patch = [];

% drawing the box, selecting a part of the plot
H = imrect(h,[]);
api = iptgetapi(H);
% getting the position of the box
position1 = api.getPosition();
% removing the now obsolete box
api.delete();

% drawing a patch in place of the first box
xlim = [position1(1) position1(1)+position1(3)];
ylim = [position1(2) position1(2)+position1(4)];
handles.patch = patch([xlim(1) xlim(2) xlim(2) xlim(1)],[ylim(1) ylim(1) ylim(2) ylim(2)],[1 1 1],'FaceColor','none');

% drawing the second box, for the inset axes
H = imrect(h,[]);
api = iptgetapi(H);
position2 = api.getPosition();
api.delete();

% converting the position from unit space to annotation space
% See: docsearch('"Positioning Annotations in Data Space"')
p2 = dsxy2figxy(h, position2);

% Creating the inset axes
handles.inset = axes('Position',p2,'box','on');%,'Nextplot','add','Layer','bottom');
% Drawing the main plot in the inset axes
for i = 1:length(handles.mainplot)
handles.insetplot(i) = plot(get(handles.mainplot(i),'XData'),get(handles.mainplot(i),'YData'),'Color',get(handles.mainplot(i),'Color'));
set(handles.inset,'NextPlot','add');
end
set(handles.inset,'NextPlot','replace');
% Zooming the inset axes to the respective box of the main plot
set(gca,'xlim',position1(1)+[0 position1(3)])
set(gca,'ylim',position1(2)+[0 position1(4)])



function varargout = dsxy2figxy(varargin)
% dsxy2figxy -- Transform point or position from axis to figure coords
% Transforms [axx axy] or [xypos] from axes hAx (data) coords into coords
% wrt GCF for placing annotation objects that use figure coords into data
% space. The annotation objects this can be used for are
%    arrow, doublearrow, textarrow
%    ellipses (coordinates must be transformed to [x, y, width, height])
% Note that line, text, and rectangle anno objects already are placed
% on a plot using axes coordinates and must be located within an axes.
% Usage: Compute a position and apply to an annotation, e.g.,
%   [axx axy] = ginput(2);
%   [figx figy] = getaxannopos(gca, axx, axy);
%   har = annotation('textarrow',figx,figy);
%   set(har,'String',['(' num2str(axx(2)) ',' num2str(axy(2)) ')'])
%
% docsearch('Positioning Annotations in Data Space')


% Obtain arguments (only limited argument checking is performed).
% Determine if axes handle is specified
if length(varargin{1})== 1 && ishandle(varargin{1}) && ...
  strcmp(get(varargin{1},'type'),'axes')	
	hAx = varargin{1};
	varargin = varargin(2:end);
else
	hAx = gca;
end;
% Parse either a position vector or two 2-D point tuples
if length(varargin)==1	% Must be a 4-element POS vector
	pos = varargin{1};
else
	[x,y] = deal(varargin{:});  % Two tuples (start & end points)
end
% Get limits
axun = get(hAx,'Units');
set(hAx,'Units','normalized');  % Need normaized units to do the xform
axpos = get(hAx,'Position');
axlim = axis(hAx);              % Get the axis limits [xlim ylim (zlim)]
axwidth = diff(axlim(1:2));
axheight = diff(axlim(3:4));
% Transform data from figure space to data space
if exist('x','var')     % Transform a and return pair of points
	varargout{1} = (x-axlim(1))*axpos(3)/axwidth + axpos(1);
	varargout{2} = (y-axlim(3))*axpos(4)/axheight + axpos(2);
else                    % Transform and return a position rectangle
	pos(1) = (pos(1)-axlim(1))/axwidth*axpos(3) + axpos(1);
	pos(2) = (pos(2)-axlim(3))/axheight*axpos(4) + axpos(2);
	pos(3) = pos(3)*axpos(3)/axwidth;
	pos(4) = pos(4)*axpos(4)/axheight;
	varargout{1} = pos;
end
% Restore axes units
set(hAx,'Units',axun)