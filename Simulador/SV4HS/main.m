% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Celina Gaiao  %  Instituto Superiror T�cnico    %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc; clear all; close all;


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Inicializa dados
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Escolher parametros de simula�ao
run('inicializa');



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        SIMULA
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sim('B_Vi_D.mdl', 10);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Trata dados
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Optar por fazer gr�ficos com legendas em Portugu�s ou Ingl�s 
% trata_dados ou trata_dados_en

% dist total
DEBUG = 2; run('trata_dados');

% grafico 3D
DEBUG = 3; run('trata_dados');

% traject�ria do Diver segundo x y z sem pontos a zeros
DEBUG  = 34; run('trata_dados');

% erro Kalman + trilatera��o   
DEBUG = 35; run('trata_dados');

% instantes de resposta dos veiculos
DEBUG = 4; run('trata_dados');      

% Falhas
DEBUG = 6; run('trata_dados');
