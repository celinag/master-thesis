% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Celina Gaiao  %  Instituto Superiror Técnico    %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc; clear all; close all;



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Inicializa dados
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('inicializa_vD');



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        SIMULA
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sim('B_Vi_D.mdl', 150);


PT = 0;
EN = 1;


 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %        Trata dados pt
 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if PT
    % dist total
    DEBUG = 2; run('trata_dados');
    % print -depsc vD_distancias_zoom

    % grafico 3D
    DEBUG = 3; run('trata_dados');
    % print -depsc vD_trajectoria_3D

    % trajectória do Diver segundo x y z sem pontos a zeros
    DEBUG  = 34; run('trata_dados');
    % print -depsc vD_trajectoria_x_zoom
    % print -depsc vD_trajectoria_x
    % print -depsc vD_trajectoria_y
    % print -depsc vD_trajectoria_z

    % erro Kalman + trilateração   
    DEBUG = 35; run('trata_dados');
    % print -depsc vD_erro_posicao
    % print -depsc vD_erro_velocidade
    % print -depsc vD_erro_covariancia
    % print -depsc vD_erro_covariancia_zoom

    % instantes de resposta dos veiculos
    DEBUG = 41; run('trata_dados');      
    % print -depsc vD_tempos_boia_diver_zoom
    % print -depsc vD_tempos_veiculos_zoom
end


 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %        Trata dados en
 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if EN
    % dist total
    DEBUG = 2; run('trata_dados_en');
    % print -depsc vD_distance_zoom

    % grafico 3D
    DEBUG = 3; run('trata_dados_en');
    % print -depsc vD_path_3D

    % trajectória do Diver segundo x y z sem pontos a zeros
    DEBUG  = 34; run('trata_dados_en');
    % print -depsc vD_trajectoria_x_zoom
    % print -depsc vD_path_x
    % print -depsc vD_trajectoria_y
    % print -depsc vD_trajectoria_z

    % erro Kalman + trilateração   
    DEBUG = 35; run('trata_dados_en');
    % print -depsc vD_position_error
    % print -depsc vD_erro_velocidade
    % print -depsc vD_erro_covariancia
    % print -depsc vD_covariance_error_zoom


    % instantes de resposta dos veiculos
    DEBUG = 41; run('trata_dados_en');      
    % print -depsc vD_tempos_boia_diver_zoom
    % print -depsc vD_tempos_veiculos_zoom

	% InsetZoomPlot
end
