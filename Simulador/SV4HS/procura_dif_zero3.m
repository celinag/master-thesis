function [indice, t, valor] = procura_dif_zero3(sinal)

    indice = []; t = []; valor = [];
    for i = 1: size(sinal.signals.values, 1)
        if sinal.signals.values(i,1) ~= 0 || sinal.signals.values(i,2) ~= 0 || sinal.signals.values(i,3) ~= 0
            indice = cat(1, indice, i);
            t = cat(1, t,sinal.time(i));
            aux = [sinal.signals.values(i,1) sinal.signals.values(i,2) sinal.signals.values(i,3)];
            valor = cat(1, valor, aux);
        end
    end
end