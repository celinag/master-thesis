
% Velocidade
% --------------------------
% xdot = Va cos(psi) +  vcx;
% ydot = Va sin(psi) +  vcy;
% zdot = 0;
% psidot = omega

Va = 0.8;           % modulo da velocidade, const != 0
omegaa = 0.005;     % posi??o angular, const != 0
zdot = 0.001;        % derivada da altura, como queremos altura const

% vcx = 0.1;          % velocidade da corrente x
% vcy = 0.1;          % velocidade da corrente y
% vcz = 0;            % velocidade da corrente z

vcx = 0.02;          % velocidade da corrente x
vcy = 0.02;          % velocidade da corrente y
vcz = 0;             % velocidade da corrente z


% tudo a zero pra testar
% Va = 0;         % modulo da velocidade, const != 0
% omegaa = 0;     % posi??o angular, const != 0
% zdot = 0;       % derivada da altura, como queremos altura const
% vcx = 0;        % velocidade da corrente x
% vcy = 0;        % velocidade da corrente y
% vcz = 0;        % velocidade da corrente z

% cinem?tica dos ve?culos
% -----------------------
% VaV = 1;
% omegaaV = 0.001;
% zdotV = 0.001;

% tudo a zero pra testar
% VaV = 0;
% omegaaV = 0;
% zdotV = 0;

% moviemnto do ve�culos semelhante ao do Diver
VaV = Va;
omegaaV = omegaa;
zdotV = 0;


% localiza?cao da boia
% --------------------------
%    x y z
% b
% boia = [100 10 -10]';

% localiza?cao dos ve?culos
% --------------------------
%    x y z
% h1
% h2
% ...
% hn
% ci_V = [10 10 -1,
%          4 150 -3,
%          110 90 -2.5,
%          -20 300 -1,
%          20 45 -1.5]; 
% 
% ci_V = [10 10 -0,
%          -10 10 -3,
%          10 -10 -2.5,
%          -10 -10 -1,
%          0 0 -1.5]; 
% 
% ci_V = [0 0 0,
%          sqrt(50) sqrt(50) 0,
%          -sqrt(50) sqrt(50) -5,
%          sqrt(50) -sqrt(50) 0,
%          -sqrt(50) -sqrt(50) 0]; 
% 
%      ci_V = [0 0 -5,
%          sqrt(50) sqrt(50) 0,
%          -sqrt(50) sqrt(50) 0,
%          sqrt(50) -sqrt(50) 0,
%          -sqrt(50) -sqrt(50) 0]; 
     
ci_V = [0 0 -2,
         sqrt(50) sqrt(50) -1,
         -sqrt(50) sqrt(50) 0,
         sqrt(50) -sqrt(50) 0,
         -sqrt(50) -sqrt(50) 0];       
     
m = size(ci_V, 1);

% velocidade do som na ?gua
vsom = 1450; %m/s

% tempo que o diver demora a responder a um ping
T_processamento_diver = 0.1;
% T_processamento_diver = 0.50;
% T_processamento_diver = 0;

% tempo que o veiculo demora responder desde que recebe o sinal do diver
T_processamento_veiculo = 0.1;
% T_processamento_veiculo = 0.30;
% T_processamento_veiculo = 0.30;


% Para o Filtro de Kalman

% Espa?o de estados
% --------------------
% Pdot = V + Vc + G(t)w1(t)
% Vcdot = 0 + w2 + w2(t)

% xdot = Ax + Bu + g(t)w1(t)
% y = Cx + v

% A = [0 1;
%      0 0];
%  
% b = [1 0];
% 
% c = [1 0];
% 
% G = [0 0;
%      0 1];
% 
% K = 1;

A = [0 0 0 1 0 0; 
     0 0 0 0 1 0;
     0 0 0 0 0 1;
     0 0 0 0 0 0;
     0 0 0 0 0 0;
     0 0 0 0 0 0];

% b = [0 0 0 0 0 0;
%      0 0 0 0 0 0;
%      0 0 0 0 0 0;
%      0 0 0 0 0 0;
%      0 0 0 0 0 0;
%      0 0 0 0 0 0];

C = [   1 0 0 0 0 0;
        0 1 0 0 0 0;
        0 0 1 0 0 0];

G = [zeros(3,6);
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1];    

% Covariancia do ruido
Q = eye(6, 6).*0.1; 
%display(Q);
R = eye(3, 3);
%display(R);


% velocidade inicial do Diver in Plant
% vai definir a posi??o inicial do mergulhador
ci_D_velocidade = [0 0 -10];

% Amostragem (pode ser alterada )
sample_time = 0.01;

% Amostragem do simulink n�o pode ser maior que 0.01
sample_time_simulink = 0.01;

% RUIDO

% Com Ruido nas distancias medidas
% ruido das medidas
ruido_media = 0;
ruido_variancia = 0.0001;
ruido_passo_inicial = 1;
ruido_periodo = 50;

% Sem Ruido
% ruido_media = 0;
% ruido_variancia = 0;



% Perdas de Mensagens
% 1 sem perdas, 2 perdas aleat?rias
comando = 1;

% Distancia m?xima a que transmite 1000m
dmax = 1000;

% DOPPLER
% 0 sem Doppler, 1 com Doppler
doppler = 0;


% Covariancia inicial
P_0 = zeros(6,6);

% Instantes em que supostamente tem medidas novas e liga o filtro SO PARA
% TESTES
% T_amostragem = 1;

% X inicial, posicao do mergulhador e velocidade para o Filtro de Kalman
X_0 = [1 -1 -11 0 0 0]';

% K inicial
K_0 = zeros(6,3);

