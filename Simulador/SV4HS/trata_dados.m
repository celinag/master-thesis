% Visualiza��o de resultados
% ---------------------------
   % l�gica dos graficos:
    % envio: positivo       recep�ao: negativo
    % acustico: tra�o fino  radio: tra�o forte
    % avisos e ping: azul   resposta: vermelho
    % x inicio de um vector o fim de um vector

% Modelo
if DEBUG == 1
    figure(); 
    % dist�ncias do submarino aos m ci_Vfones
    plot(dist.time, dist.signals.values(:,1),'b'); 
    hold on;
    plot(dist.time, dist.signals.values(:,2),'r'); 
    plot(dist.time, dist.signals.values(:,3),'g'); 
    plot(dist.time, dist.signals.values(:,4),'c'); 
    plot(dist.time, dist.signals.values(:,5),'y'); 
    xlabel('Tempo'); ylabel('distancia medida');
    title('distancia do submarino aos m ci_Vfones ');   
    
    figure();
    % ci_Vfones
%     plot3(ci_V(:,1), ci_V(:,2), ci_V(:,3),'ro');
    plot3(ci_V(1,1), ci_V(1,2), ci_V(1,3),'bo', 'LineWidth',2);
    hold on; grid on;
    plot3(ci_V(2,1), ci_V(2,2), ci_V(2,3),'ro', 'LineWidth',2);
    plot3(ci_V(3,1), ci_V(3,2), ci_V(3,3),'go', 'LineWidth',2);
    plot3(ci_V(4,1), ci_V(4,2), ci_V(4,3),'co', 'LineWidth',2);
    plot3(ci_V(5,1), ci_V(5,2), ci_V(5,3),'yo', 'LineWidth',2);
    title('Modelo');

    % trajectoria posi��o real
    plot3(P_D.signals.values(:,1), P_D.signals.values(:,2), P_D.signals.values(:,3), 'b');
    xlabel('x'); ylabel('y'), zlabel('z');
    legend('boia', 'v1', 'v2', 'v3', 'v4', 'diver', 'Location', 'Best');
end

% Distancias
if DEBUG == 2
    figure(); 
    % dist�ncias do submarino aos m ci_Vfones
    plot(dist.time, dist.signals.values(:,1),'g'); 
    hold on; grid on;
    plot(dist.time, dist.signals.values(:,2),'r'); 
    plot(dist.time, dist.signals.values(:,3),'b'); 
    plot(dist.time, dist.signals.values(:,4),'c'); 
    plot(dist.time, dist.signals.values(:,5),'y'); 

    % dist�ncias do submarino aos m ci_Vfones
    plot(dist_exp.time, dist_exp.signals.values(:,1),'og'); 
    plot(dist_exp.time, dist_exp.signals.values(:,2),'or'); 
    plot(dist_exp.time, dist_exp.signals.values(:,3),'ob'); 
    plot(dist_exp.time, dist_exp.signals.values(:,4),'oc'); 
    plot(dist_exp.time, dist_exp.signals.values(:,5),'oy'); 
    xlabel('Tempo [s]'); ylabel('Dist�ncia [m]');
    title('Dist�ncia real e estimada do Mergulhador aos Ve�culos');
    legend('V1 real', 'V2 real','V3 real','V4 real','V5 real','V1 estimada','V2 estimada','V3 estimada','V4 estimada','V5 estimada', 'Location', 'Best');
end

% Movimento
if DEBUG == 3
    % Figura com a trajectoria real e com a estimada 3D
    figure();
    % Hidrofones
%     plot3(ci_V(:,1), ci_V(:,2), ci_V(:,3),'ro');
    plot3(ci_V(1,1), ci_V(1,2), ci_V(1,3),'bx', 'LineWidth',2);
    hold on; grid on;
    plot3(ci_V(2,1), ci_V(2,2), ci_V(2,3),'rx', 'LineWidth',2);
    plot3(ci_V(3,1), ci_V(3,2), ci_V(3,3),'gx', 'LineWidth',2);
    plot3(ci_V(4,1), ci_V(4,2), ci_V(4,3),'cx', 'LineWidth',2);
    plot3(ci_V(5,1), ci_V(5,2), ci_V(5,3),'yx', 'LineWidth',2);
    title('Modelo');
%     
    plot3(P_V1.signals.values(:,1), P_V1.signals.values(:,2), P_V1.signals.values(:,3),'b');
    plot3(P_V2.signals.values(:,1), P_V2.signals.values(:,2), P_V2.signals.values(:,3),'r');
    plot3(P_V3.signals.values(:,1), P_V3.signals.values(:,2), P_V3.signals.values(:,3),'g');
    plot3(P_V4.signals.values(:,1), P_V4.signals.values(:,2), P_V4.signals.values(:,3),'c');
    plot3(P_V5.signals.values(:,1), P_V5.signals.values(:,2), P_V5.signals.values(:,3),'y');
    
    % trajectoria real
%     i_n_z = 1274;   % indice diferente de zero pro grafico se + faclmente visivel
    i_n_z = 1;
    
    % REAL
    % inicio do vector
    plot3(P_D.signals.values(1,1), P_D.signals.values(1,2), P_D.signals.values(1,3), 'xb');
    % fim do vector
    plot3(P_D.signals.values(end,1), P_D.signals.values(end,2), P_D.signals.values(end,3), 'ob');
    % vector a partir de i_n_z
    plot3(P_D.signals.values(i_n_z:end,1), P_D.signals.values(i_n_z:end,2), P_D.signals.values(i_n_z:end,3), 'ob');
    
    % MEDIDO
    % inicio do vector
    plot3(pm.signals.values(1,1), pm.signals.values(1,2), pm.signals.values(1,3), 'xr');
    % fim do vector
    plot3(pm.signals.values(end,1), pm.signals.values(end,2), pm.signals.values(end,3), 'or');
    % vector a partir de i_n_z
    plot3(pm.signals.values(i_n_z:end,1), pm.signals.values(i_n_z:end,2), pm.signals.values(i_n_z:end,3), 'or');
    
    % ESTIMADO
    % inicio do vector
    plot3(pest.signals.values(1,1), pest.signals.values(1,2), pest.signals.values(1,3), 'xg');
    % fim do vector
    plot3(pest.signals.values(end,1), pest.signals.values(end,2), pest.signals.values(end,3), 'og');
    % vector a partir de i_n_z
    plot3(pest.signals.values(i_n_z:end,1), pest.signals.values(i_n_z:end,2), pest.signals.values(i_n_z:end,3), 'og');
    
    xlabel('x [m]'); ylabel('y [m]'), zlabel('z [m]');
%     legend('boias', 'diver', 'diver est', 'Location', 'Best');
%     axis([-50 200 -50 350 -20 0]);
    axis([-50 200 -50 350 -20 10]);
    legend('V1, Principal', 'V2', 'V3', 'V4', 'V5', 'Real', 'Trilatera��o', 'Kalman', 'Location', 'Best');

% Trajectoria real e estimada 2D
%     figure();
%     plot(P_D.signals.values(1,1), P_D.signals.values(1,2), 'ob'); hold on; grid on;
%     plot(P_D.signals.values(:,1), P_D.signals.values(:,2), 'b'); hold on; grid on;
%     plot(pm.signals.values(:,1), pm.signals.values(:,2), '+r');
%     legend('real', 'estimada');  xlabel('x'); ylabel('y');
%     title('traject�ria do Diver');
%     
%     figure();
%     plot(P_D.signals.values(1,1), P_D.signals.values(1,3), 'ob'); hold on; grid on;
%     plot(P_D.signals.values(:,1), P_D.signals.values(:,3), 'b'); hold on; grid on;
%     plot(pm.signals.values(:,1), pm.signals.values(:,3), '+r');
%     legend('real', 'estimada');  xlabel('x'); ylabel('z');
%     title('traject�ria do Diver');
end

if DEBUG == 33
% Trajectoria real e estimada em funcao do tempo
    figure();
    plot(P_D.time, P_D.signals.values(:,1), 'ob'); hold on; grid on;
    plot(pest.time, pest.signals.values(:,1), 'xg');
    plot(pm.time, pm.signals.values(:,1), 'xr');
    legend('Real', 'Kalman', 'trilateration');  xlabel('Tempo'); ylabel('x position');
    title('Traject�ria do Diver real e estimada segundo X');
          
    figure();
    plot(pm.time, P_D.signals.values(:,2), 'ob'); hold on; grid on;
    plot(pest.time, pest.signals.values(:,2), 'xg');
    plot(pm.time, pm.signals.values(:,2), 'xr');
    legend('Real', 'Kalman', 'trilateration');  xlabel('time'); ylabel('y position');
    %title('Traject�ria do Diver real e estimada segundo Y');
    title('Driver''s path');
         
    figure();
    plot(pm.time, P_D.signals.values(:,3), 'ob'); hold on; grid on;
    plot(pest.time, pest.signals.values(:,3), 'xg');
    plot(pm.time, pm.signals.values(:,3), 'xr');
    legend('Real', 'Kalman', 'trilateration');  xlabel('time'); ylabel('z position');
    title('Traject�ria do Diver real e estimada segundo Z');
end

if DEBUG == 34
% Trajectoria real e estimada em funcao do tempo
    [pm_indice, pm_t, pm_valor] = procura_dif_zero3(pm);
    
    figure();
    plot(P_D.time, P_D.signals.values(:,1), 'ob'); hold on; grid on;
    plot(pest.time, pest.signals.values(:,1), 'xg');
    plot(pm_t, pm_valor(:,1), 'ro','LineWidth', 1);
    legend('Real', 'Kalman', 'Trilatera��o', 'Location', 'Best');  
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Mergulhador: Real e Estimada, em x');
          
    figure();
    plot(P_D.time, P_D.signals.values(:,2), 'ob'); hold on; grid on;
    plot(pest.time, pest.signals.values(:,2), 'xg');
    plot(pm_t, pm_valor(:,2), 'ro','LineWidth', 1)
    legend('Real', 'Kalman', 'Trilatera��o', 'Location', 'Best');  
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Mergulhador: Real e Estimada, em y');
%     title('Driver''s path');
         
    figure();
    plot(P_D.time, P_D.signals.values(:,3), 'ob'); hold on; grid on;
    plot(pest.time, pest.signals.values(:,3), 'xg');
    plot(pm_t, pm_valor(:,3),'ro','LineWidth', 1);
    legend('Real', 'Kalman', 'Trilatera��o', 'Location', 'Best');  
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Mergulhador: Real e Estimada, em z');
end

if DEBUG == 345
% Trajectoria real e estimada em funcao do tempo
    [pm_indice, pm_t, pm_valor] = procura_dif_zero3(pm);
    
    figure();
    plot(P_D.time, P_D.signals.values(:,1), 'ob'); hold on; grid on;
    plot(pm_t, pm_valor(:,1), 'ro','LineWidth', 1);
    plot(pest.time, pest.signals.values(:,1), 'xg');
    legend('Real', 'Trilatera��o', 'Kalman', 'Location', 'Best');  
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Mergulhador: Real e Estimada, em x');
          
    figure();
    plot(P_D.time, P_D.signals.values(:,2), 'ob'); hold on; grid on;
    plot(pm_t, pm_valor(:,2), 'ro','LineWidth', 1)
    plot(pest.time, pest.signals.values(:,2), 'xg');
    legend('Real', 'Trilatera��o','Kalman', 'Location', 'Best');  
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Mergulhador: Real e Estimada, em y');
%     title('Driver''s path');
         
    figure();
    plot(P_D.time, P_D.signals.values(:,3), 'ob'); hold on; grid on;
    plot(pm_t, pm_valor(:,3),'ro','LineWidth', 1);
    plot(pest.time, pest.signals.values(:,3), 'xg');
    legend('Real', 'Trilatera��o', 'Kalman', 'Location', 'Best');  
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Mergulhador: Real e Estimada, em z');
end

if DEBUG == 35
% Xtilde, Erro de Kalman acomulado com o erro da trilatera��o
    figure(); 
    plot(xtilde.time, xtilde.signals.values(:,1),'b'); 
    hold on; grid on;
    plot(xtilde.time, xtilde.signals.values(:,2),'r'); 
    plot(xtilde.time, xtilde.signals.values(:,3),'g'); 
    xlabel('Tempo [s]'); ylabel('Erro da Posi��o [m]');
    title('Erro na estimativa da Posi��o'); 
    legend('x', 'y', 'z', 'Location', 'Best'); 
    
    figure(); 
    plot(xtilde.time, xtilde.signals.values(:,4),'b'); 
    hold on; grid on;
    plot(xtilde.time, xtilde.signals.values(:,5),'r'); 
    plot(xtilde.time, xtilde.signals.values(:,6),'g'); 
    xlabel('Tempo [s]'); ylabel('Erro da Velocidade [m/s]');
    title('Erro na estimativa da Velocidade');   
    legend('vx', 'vy', 'vz', 'Location', 'Best');
    
    % Tra�o da covari�ncia
    tamanho = size(Covariancia.signals.values, 3);
    traco = zeros(tamanho, 1);
    for i=1:tamanho
        traco(i) = trace(Covariancia.signals.values(:,:,i));
    end
    figure(); hold on; grid on;
    plot(Covariancia.time, traco, 'b');
    xlabel('Tempo [s]'); ylabel('Tra�o da Covari�ncia');
    title('Tra�o da Covari�ncia do Erro de Kalman');
end

% Movimento mas com anima��o
if DEBUG == 333
    figure();
   
    plot3(ci_V(1,1), ci_V(1,2), ci_V(1,3),'bx', 'LineWidth',2);
    hold on; grid on;
    plot3(ci_V(2,1), ci_V(2,2), ci_V(2,3),'rx', 'LineWidth',2);
    plot3(ci_V(3,1), ci_V(3,2), ci_V(3,3),'gx', 'LineWidth',2);
    plot3(ci_V(4,1), ci_V(4,2), ci_V(4,3),'cx', 'LineWidth',2);
    plot3(ci_V(5,1), ci_V(5,2), ci_V(5,3),'yx', 'LineWidth',2);
    
    for i=1:10:size(P_D.signals.values, 1)
        % Veiculos
        plot3(hidro1.signals.values(i,1), hidro1.signals.values(i,2), hidro1.signals.values(i,3),'-r', 'LineWidth',1);
        plot3(hidro2.signals.values(i,1), hidro2.signals.values(i,2), hidro2.signals.values(i,3),'-g', 'LineWidth',1);
        plot3(hidro3.signals.values(i,1), hidro3.signals.values(i,2), hidro3.signals.values(i,3),'-c', 'LineWidth',1);
        plot3(hidro4.signals.values(i,1), hidro4.signals.values(i,2), hidro4.signals.values(i,3),'-y', 'LineWidth',1);
    
        % Diver
        plot3(P_D.signals.values(i,1), P_D.signals.values(i,2), P_D.signals.values(i,3), '-b', 'LineWidth',1);
        plot3(pm.signals.values(i,1), pm.signals.values(i,2), pm.signals.values(i,3), '-r', 'LineWidth',1);
        pause(0.0001);
        
        axis([-50 200 -50 350 -20 0]);
    end
    legend('Boia', 'V1', 'V2', 'V3', 'V4', ' ', ' ', ' ', ' ','D Real', 'C est');
    title('Modelo');    
end

% Comunica��es
if DEBUG == 4
    % l�gica dos graficos:
    % envio: positivo       recep�ao: negativo
    % acustico: tra�o fino  radio: tra�o forte
    % avisos e ping: azul   resposta: vermelho
    
    % BOIA
    figure(); 
    subplot(2,1,1);
    [auxx, auxy] = procura_flancos(avisa_veiculos, 1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;

    [auxx, auxy] = procura_flancos(envia_ping, 1);
    stem(auxx, auxy, 'xb', 'LineWidth',1);
    
    [auxx, auxy] = procura_flancos(t_recepcao_resposta, -1);
    stem(auxx, auxy, '-r*', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    legend('1: Avisa Ve�culos - R', '2: Ping ao Mergulhador - A', '6: Recebe resposta - A');
    title('Troca de Mensagens do Ve�culo Principal');
    
    % DIVER
    subplot(2,1,2);
    [auxx, auxy] = procura_flancos(t_recepcao_ping, -1);
    stem(auxx, auxy, '-b*', 'LineWidth',1); hold on; grid on;
    
    t_envio_resposta_aux.time = t_envio_resposta.time;
    t_envio_resposta_aux.signals.values = t_envio_resposta.signals.values(:,2);
    
    [auxx, auxy] = procura_flancos(t_envio_resposta_aux, 1);
    stem(auxx, auxy, 'or', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    legend('4: Recebe ping - A', '5: Envia resposta - A');
    title('Troca de Mensagens do Mergulhador');
    
    % VEICULOS
    figure();
    subplot(4,1,1);
    [auxx, auxy] = procura_flancos(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos(recebe_acustico2, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto1] = procura_flancos(responde_v2, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    %     legend('Recebe aviso - R', 'Recebe resposta - A', 'Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 2');
    
    subplot(4,1,2);
    [auxx, auxy] = procura_flancos(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos(recebe_acustico3, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto2] = procura_flancos(responde_v3, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    %     legend('Recebe aviso - R', 'Recebe resposta - A', 'Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 3');
    
    subplot(4,1,3);
    [auxx, auxy] = procura_flancos(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos(recebe_acustico4, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto3] = procura_flancos(responde_v4, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    %     legend('Recebe aviso - R', 'Recebe resposta - A', 'Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 4');
    
    subplot(4,1,4);
    [auxx, auxy] = procura_flancos(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos(recebe_acustico5, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto4] = procura_flancos(responde_v5, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    legend('3: Recebe aviso - R', '6: Recebe resposta - A', '7: Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 5'); 
end

if DEBUG == 41
    % l�gica dos graficos:
    % envio: positivo       recep�ao: negativo
    % acustico: tra�o fino  radio: tra�o forte
    % avisos e ping: azul   resposta: vermelho
    
    % BOIA
    figure(); 
    subplot(2,1,1);
    [auxx, auxy] = procura_flancos_tempo(avisa_veiculos, 1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;

    [auxx, auxy] = procura_flancos_sem_zeros(envia_ping, 1);
    stem(auxx, auxy, '-.b', 'LineWidth',1);
    
    [auxx, auxy] = procura_flancos_tempo(t_recepcao_resposta, -1);
    stem(auxx, auxy, '-r*', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    legend('1: Avisa Ve�culos - R', '2: Ping ao Mergulhador - A', '6: Recebe resposta - A');
    title('Troca de Mensagens do Ve�culo Principal');
    
    % DIVER
    subplot(2,1,2);
    [auxx, auxy] = procura_flancos_tempo(t_recepcao_ping, -1);
    stem(auxx, auxy, '-b*', 'LineWidth',1); hold on; grid on;
    
    t_envio_resposta_aux.time = t_envio_resposta.time;
    t_envio_resposta_aux.signals.values = t_envio_resposta.signals.values(:,2);
    
    [auxx, auxy] = procura_flancos_tempo(t_envio_resposta_aux, 1);
    stem(auxx, auxy, 'or', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    legend('4: Recebe ping - A', '5: Envia resposta - A');
    title('Troca de Mensagens do Mergulhador');
    
    % VEICULOS
    figure();
    subplot(4,1,1);
    [auxx, auxy] = procura_flancos_tempo(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos_tempo(recebe_acustico2, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto1] = procura_flancos_sem_zeros(responde_v2, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    %     legend('Recebe aviso - R', 'Recebe resposta - A', 'Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 2');
    
    subplot(4,1,2);
    [auxx, auxy] = procura_flancos_tempo(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos_tempo(recebe_acustico3, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto2] = procura_flancos_sem_zeros(responde_v3, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    %     legend('Recebe aviso - R', 'Recebe resposta - A', 'Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 3');
    
    subplot(4,1,3);
    [auxx, auxy] = procura_flancos_tempo(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos_tempo(recebe_acustico4, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto3] = procura_flancos_sem_zeros(responde_v4, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    %     legend('Recebe aviso - R', 'Recebe resposta - A', 'Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 4');
    
    subplot(4,1,4);
    [auxx, auxy] = procura_flancos_tempo(recebe_aviso, -1);
    stem(auxx, auxy, 'o--c', 'LineWidth',1); hold on; grid on;
    [auxx, auxy] = procura_flancos_tempo(recebe_acustico5, -1);
    stem(auxx, auxy, 'r*', 'LineWidth',1);
    [auxx, auxy, resto4] = procura_flancos_sem_zeros(responde_v5, 1);
    stem(auxx, auxy, 'o--b', 'LineWidth',1);
    xlabel('Tempo [s]'); ylabel('Sinal');
    legend('3: Recebe aviso - R', '6: Recebe resposta - A', '7: Envia calculo - R');
    title('Troca de Mensagens do Ve�culo 5'); 
end


if DEBUG == 5
    if doppler
        % se o Diver tiver Doppler o filtro estima a velocidade da corrente
        figure();               
        subplot(3,1,1); plot(v_corrente.time, v_corrente.signals.values(:,1), 'b'); hold on;
                        plot(vel_corrente_est.time, vel_corrente_est.signals.values(:,1), 'r'); title('V da corrente x');
        subplot(3,1,2); plot(v_corrente.time, v_corrente.signals.values(:,2), 'b'); hold on;
                        plot(vel_corrente_est.time, vel_corrente_est.signals.values(:,2), 'r'); title('V da corrente y');
        subplot(3,1,3); plot(v_corrente.time, v_corrente.signals.values(:,3), 'b'); hold on;
                        plot(vel_corrente_est.time, vel_corrente_est.signals.values(:,3), 'r');  title('V da corrente z');
    else
        % se o Diver n�o tiver Doppler o filtro estima a velocidade do Diver
        figure();
        subplot(3,1,1); plot(v_inercial.time, v_inercial.signals.values(:,1), 'b'); hold on;
                        plot(vel_corrente_est.time, vel_corrente_est.signals.values(:,1), 'r'); title('V do Diver x');
        subplot(3,1,2); plot(v_inercial.time, v_inercial.signals.values(:,2), 'b'); hold on;
                        plot(vel_corrente_est.time, vel_corrente_est.signals.values(:,2), 'r'); title('V do Diver y');
        subplot(3,1,3); plot(v_inercial.time, v_inercial.signals.values(:,3), 'b'); hold on;
                        plot(vel_corrente_est.time, vel_corrente_est.signals.values(:,3), 'r');  title('V do Diver z');
    end
end

if DEBUG == 6    
    % Falhas injectadas no sistema pelo bloco de gera��o de perdas
    % com 1 no eixo do y em caso de falha
    t_inicial = 0;
    t_final = 600;
    figure();
    axis([t_inicial t_final 0 2]);
    [indice2, t, valor] = procura_dif_zero(t_erro_acustico2);
    valor = ones(size(valor));
    subplot(4,1,1); plot(t, valor, 'rx','LineWidth',1.2);
    xlabel('Tempo [s]');
    title('Perda de mensagens para V2'); grid on;

    axis([t_inicial t_final 0 2]);
    [indice3, t, valor] = procura_dif_zero(t_erro_acustico3);
    valor = ones(size(valor));
    subplot(4,1,2); plot(t, valor, 'rx','LineWidth',1.2);
    xlabel('Tempo [s]');
    title('Perda de mensagens para V3'); grid on;
        axis([t_inicial t_final 0 2]);

    [indice4, t, valor] = procura_dif_zero(t_erro_acustico4);
    valor = ones(size(valor));
    subplot(4,1,3); plot(t, valor, 'rx','LineWidth',1.2);
    xlabel('Tempo [s]');
    title('Perda de mensagens para V4'); grid on;
        
    axis([t_inicial t_final 0 2]);
    [indice5, t, valor] = procura_dif_zero(t_erro_acustico5);
    valor = ones(size(valor));
    subplot(4,1,4); plot(t, valor, 'rx','LineWidth',1.2);
    xlabel('Tempo [s]');
    title('Perda de mensagens para V5'); grid on;
    
    % Threshod da fun��o de erro
    maior_distancia = max([max(dist.signals.values(:,1)),max(dist.signals.values(:,2)),max(dist.signals.values(:,3)),max(dist.signals.values(:,4)),max(dist.signals.values(:,5))]);
    distancias = [0:0.1:maior_distancia];
    Threshold = 1-exp(distancias./(distancias - dmax));
    figure();
    plot(distancias, Threshold,'LineWidth',2); hold on; 
    xlabel('Dist�ncia [m]'); ylabel('Threshold'); 
    title('Perda de Mensagens em Fun��o da Dist�ncia'); grid on;
end

if DEBUG == 61
    % print de instantes de falhas e valor de threshold caso haja falhas
    % aleatorias
    
    disp('Falhas de comunica��o detectadas em V2'); disp(falhas_n.signals.values(end,1));
    disp('Falhas de comunica��o detectadas em V3'); disp(falhas_n.signals.values(end,2));
    disp('Falhas de comunica��o detectadas em V4'); disp(falhas_n.signals.values(end,3));
    disp('Falhas de comunica��o detectadas em V5'); disp(falhas_n.signals.values(end,4));  
    
    % falhas detectadas no master, depois de ocorrer timeout
    figure(); 
    subplot(4,1,1); plot(falhas_inst.time, falhas_inst.signals.values(:,1), 'ro'); 
    xlabel('Tempo [s]');
    title('Perda de mensagens de V2'); grid on;
    
    subplot(4,1,2); plot(falhas_inst.time, falhas_inst.signals.values(:,2), 'ro'); 
    xlabel('Tempo [s]');
    title('Perda de mensagens de V3'); grid on;
    
    subplot(4,1,3); plot(falhas_inst.time, falhas_inst.signals.values(:,3), 'ro');
    xlabel('Tempo [s]');
    title('Perda de mensagens de V4'); grid on;
    
    subplot(4,1,4); plot(falhas_inst.time, falhas_inst.signals.values(:,4), 'ro'); 
    xlabel('Tempo [s]');
    title('Perda de mensagens de V5'); grid on;

    % Falhas injectadas no sistema pelo bloco de gera��o de perdas
    figure(); 
    subplot(4,1,1); plot(t_erro_acustico2.time, t_erro_acustico2.signals.values, 'ro');
    xlabel('Tempo [s]'); ylabel('Falha');
    title('Perda de mensagens para V2'); grid on;
    
    subplot(4,1,2); plot(t_erro_acustico3.time, t_erro_acustico3.signals.values, 'ro');
    xlabel('Tempo [s]'); ylabel('Falha');
    title('Perda de mensagens para V3'); grid on;
    
    subplot(4,1,3); plot(t_erro_acustico4.time, t_erro_acustico4.signals.values, 'ro');
    xlabel('Tempo [s]'); ylabel('Falha');
    title('Perda de mensagens para V4'); grid on;
    
    subplot(4,1,4); plot(t_erro_acustico5.time, t_erro_acustico5.signals.values, 'ro'); 
    xlabel('Tempo [s]'); ylabel('Falha');
    title('Perda de mensagens para V5'); grid on;
end
