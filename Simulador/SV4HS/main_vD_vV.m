% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Celina Gaiao  %  Instituto Superiror Técnico    %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc; clear all; close all;



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Inicializa dados
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('inicializa_vD_vV');



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        SIMULA
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sim('B_Vi_D.mdl', 600);



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Trata dados
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% dist total
DEBUG = 2; run('trata_dados');
% print -depsc vD_vV_distancias

% grafico 3D
DEBUG = 3; run('trata_dados');
% print -depsc vD_vV_trajectoria_3D

% trajectória do Diver segundo x y z sem pontos a zeros
DEBUG  = 345; run('trata_dados');
% print -depsc vD_vV_trajectoria_x_zoom
% print -depsc vD_vV_trajectoria_x
% print -depsc vD_vV_trajectoria_y
% print -depsc vD_vV_trajectoria_z

% erro Kalman + trilateração   
DEBUG = 35; run('trata_dados');
% print -depsc vD_vV_erro_posicao
% print -depsc vD_vV_erro_velocidade
% print -depsc vD_vV_erro_covariancia
% print -depsc vD_vV_erro_covariancia_zoom

% instantes de resposta dos veiculos
% DEBUG = 4; run('trata_dados');      
% print -depsc vD_vV_tempos_boia_diver_zoom
% print -depsc vD_vV_tempos_veiculos_zoom

% InsetZoomPlot

