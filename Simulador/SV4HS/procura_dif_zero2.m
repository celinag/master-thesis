function [indice, t, valor] = procura_dif_zero2(sinal)

indice = []; t = []; valor = [];
    for i = 1: size(sinal.signals.values, 1)
        if sinal.signals.values(i,2) ~= 0
            indice = cat(1, indice, i);
            t = cat(1, t,sinal.time(i));
            valor = cat(1, valor, sinal.signals.values(i,2));
        end
    end
end