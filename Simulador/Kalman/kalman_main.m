%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Filtro de Kalman * Continuo * Discreto * Hibrido
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all; clc;

kc = 0; % Kalman continuo
kd = 0; % Kalman discretizado a partir do continuo, as matrizes mantem-se iguais
kdo = 0;% kalmana discreto original, matrizes discretizadas
kh = 1; % Kalman h?brido

disp('*****Dados Comuns*****');

% Modelo
% xdot = Ax + Bu + g(t)w1(t)
% y = Cx + v
A = [0 0 1 0; 
     0 0 0 1;
     0 0 0 0;
     0 0 0 0];

C = [1 0 0 0;
     0 1 0 0];

G = [zeros(2,4);
     0 0 1 0;
     0 0 0 1];    

% Covariancia do ruido
Q = eye(4, 4).*0.5; 
R = eye(2, 2);


% Ganho de Kalman
% ----------------

% eq de Riccati
% [X,L,G] = care(A,B,Q)
% x covari�ncia
% l vector L of closed-loop eigenvalues, where L=eig(A-B*G,E)
% g the gain matrix G =R^-1(B'XE+S')

[x,l,g] = care(A',C',G*Q*G',R);

k = x*C'*R^-1; display(k);

k1 = k(1,1); display(k1);
k2 = k(3,1); display(k2);


% Condi??es iniciais integradores do Filtro Kalman
vcdotest0 = [0 0]; 
pdotest0 = [0 0];

% X inicial, posicao do mergulhador e velocidade
X_0 = [0 0 0 0]';

% Plant
% --------------------------
% xdot = Va cos(psi*t);
% ydot = Va sin(psi*t);
% zdot = 0;
% psidot = omega

Va = 0.5;           % modulo da velocidade, const != 0
omegaa = 0.03;      % posi??o angular, const != 0

% velocidade inicial do Diver in Plant
% vai definir a posi??o inicial do mergulhador
ci_D_velocidade = [1 1];

% amostragem
sample_time = 0.001;

% leitura de resposta
T_leitura = sample_time*50;

% ruido das medidas
ruido_media = 0;
ruido_variancia = 0.001;
ruido_passo_inicial = 70;
ruido_periodo = sample_time*70;

% instantes de tempo em que se t?m medidas novas
T_amostragem = 3;

% covariancia inicial
P_0 = 0.1*[1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];

% tempo de simulacao
tempo_simulacao = 30;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Kalman continuo
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if kc
    % funciona bem, mesmo com ruido
    disp('*****kalman continuo*****');
    sim('kalman_continuo', tempo_simulacao);

    % gr?fico real vs estimado
    figure(); hold on; grid on;
    plot(P_D.signals.values(:,1), P_D.signals.values(:,2), 'b', 'LineWidth', 1.5);
    plot(P_D_erro.signals.values(:,1), P_D_erro.signals.values(:,2), 'r', 'LineWidth', 1.5);
    plot(P_D_Kalman.signals.values(:,1), P_D_Kalman.signals.values(:,2), 'g', 'LineWidth', 1.5);

    legend('Posi��o Real', 'Posi��o Medida', 'Estimativa de Kalman');
    xlabel('x [m]'); ylabel('y [m]');
    title('Traject�ria do Objecto');

    % gr?fico com o erro de kalman
    figure(); hold on; grid on;
    plot(erro_kalman.time, erro_kalman.signals.values(:,1), 'b', 'LineWidth',1.5);
    plot(erro_kalman.time, erro_kalman.signals.values(:,2), 'g', 'LineWidth',1.5);

    legend('erro x', 'erro y');
    xlabel('Tempo [s]'); ylabel('Erro [m]');
    title('Erro na estimativa da Posi��o do Objecto');
    
    % FUN��ES DE TRANSFER�NCIA
    [x,l,g] = care(A',C',G*Q*G',R);
    k = x*C'*R^-1; display(k);
    k1_1 = k(1,1); display(k1_1);
    k2_1 = k(3,1); display(k2_1);
    
    R_2 = eye(2, 2)*0.1;
    [x,l,g] = care(A',C',G*Q*G',R_2);
    k = x*C'*R_2^-1; display(k);
    k1_2 = k(1,1); display(k1_2);
    k2_2 = k(3,1); display(k2_2);
    
    R_3 = eye(2, 2)*5;
    [x,l,g] = care(A',C',G*Q*G',R_3);
    k = x*C'*R_3^-1; display(k);
    k1_3 = k(1,1); display(k1_3);
    k2_3 = k(3,1); display(k2_3);
    
    
    % fun��o de transfer�ncia de Pm para Pest
    disp('Pest/Pm');
    num = [k1_1 k2_1]; den = [1 k1_1 k2_1];
    sys = tf(num, den); display(sys);
    figure(); bode(sys); title('Pm - Pest'); hold on; grid on;
    
    disp('Pest/Pm');
    num = [k1_2 k2_2]; den = [1 k1_2 k2_2];
    sys = tf(num, den); display(sys);
    bode(sys);
    
    disp('Pest/Pm');
    num = [k1_3 k2_3]; den = [1 k1_3 k2_3];
    sys = tf(num, den); display(sys);
    bode(sys);
    legend('k1=1.189, k2=0.707','k1=2.115, k2=2.236','k1=0.795, k2=0.316');
    
    % print -depsc kalman_continuo
    % print -depsc kalman_continuo_erro
    % print -depsc funcao_transferencia

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Kalman discreto (discretizado a partir do continuo)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if kd
    % funciona bem, mesmo com ruido
    disp('*****kalman discretizado*****');
    sim('kalman_discretizado', tempo_simulacao);

    % gr?fico real vs estimado
    figure(); hold on; grid on;
    plot(P_D.signals.values(:,1), P_D.signals.values(:,2), 'bo', 'LineWidth', 1);
    plot(P_D_Kalman.signals.values(:,1), P_D_Kalman.signals.values(:,2), 'gx', 'LineWidth', 1);
    plot(P_D_Saltos.signals.values(:,1), P_D_Saltos.signals.values(:,2), 'ro', 'LineWidth', 2);


    legend('Posi��o Real', 'Estimativa de Kalman', 'Posi��o Medida');
    xlabel('x [m]'); ylabel('y [m]');
    title('Traject�ria do Objecto');
    
    % gr�fico x e y em fun��o do tempo
    figure(); hold on; grid on;
    plot(P_D.time, P_D.signals.values(:,1), 'bo','LineWidth',0.1);   
    plot(P_D.time, P_D.signals.values(:,2), 'b+','LineWidth',1);
    plot(P_D_Kalman.time, P_D_Kalman.signals.values(:,1), 'g','LineWidth',2);
    plot(P_D_Kalman.time, P_D_Kalman.signals.values(:,2), 'g--','LineWidth',2);
    plot(P_D_Saltos.time, P_D_Saltos.signals.values(:,1), 'ro','LineWidth',2);
    plot(P_D_Saltos.time, P_D_Saltos.signals.values(:,2), 'r+','LineWidth',2);
    legend('Posicao Real x', 'Posicao Real y','Estimativa de Kalman x', 'Estimativa de Kalman y', 'Posi��o Medida x', 'Posi��o Medida y');
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Objecto ao longo do tempo');

    % gr?fico com o erro de kalman
    figure(); hold on; grid on;
    plot(erro_kalman_discreto.time, erro_kalman_discreto.signals.values(:,1), 'b', 'LineWidth', 1.5);
    plot(erro_kalman_discreto.time, erro_kalman_discreto.signals.values(:,2), 'g', 'LineWidth', 1.5);

    legend('erro x', 'erro y');
    xlabel('Tempo [s]'); ylabel('Erro [m]');
    title('Erro na estimativa da Posi��o do Objecto');
    
%     % dados recebidos no tempo
%     figure(); hold on; grid on;
%     plot(P_D_Saltos.time, P_D_Saltos.signals.values(:,1), 'bo');
% 
%     legend('x');
%     xlabel('tempo'); ylabel('medidas x');
%     title('medidas');
%     
%     % gr?fico real vs estimado
%     figure(); hold on; grid on;
%    
%     plot(P_D_Kalman.time, P_D_Kalman.signals.values(:,1), 'rx');
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Kalman discreto original
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if kdo
    disp('*****kalman discreto*****');

    % Modelo
    % xdot = Ax + Bu + g(t)w1(t)
    % y = Cx + v
    A = [0 0 1 0; 
         0 0 0 1;
         0 0 0 0;
         0 0 0 0];

    B = zeros(4,1);

    C = [1 0 0 0;
         0 1 0 0];

    D = zeros(2,1);

    G = [zeros(2,4);
         0 0 1 0;
         0 0 0 1];
     
    SYS = ss(A, B, C, D); display(SYS);
    SYSd = c2d(SYS, sample_time); display(SYSd);
    
    [Ad Bd Cd Dd] = ssdata(SYSd); % para ir buscar as matrizes de estado discretas

    sim('kalman_discreto', tempo_simulacao);

    % gr?fico real vs estimado
    figure(); hold on; grid on;
    plot(P_D.signals.values(:,1), P_D.signals.values(:,2), 'bo','LineWidth',1);
    plot(P_D_Kalman.signals.values(:,1), P_D_Kalman.signals.values(:,2), 'gx','LineWidth',1);
    plot(P_D_saltos_erros.signals.values(:,1), P_D_saltos_erros.signals.values(:,2), 'ro','LineWidth',2);

    legend('Posicao Real', 'Estimativa de Kalman', 'Posi��o Medida');
    xlabel('x [m]'); ylabel('y [m]');
    title('Traject�ria do Objecto');

    % gr�fico x e y em fun��o do tempo
    figure(); hold on; grid on;
    plot(P_D.time, P_D.signals.values(:,1), 'bo','LineWidth',0.1);   
    plot(P_D.time, P_D.signals.values(:,2), 'b+','LineWidth',1);
    plot(P_D_Kalman.time, P_D_Kalman.signals.values(:,1), 'g','LineWidth',2);
    plot(P_D_Kalman.time, P_D_Kalman.signals.values(:,2), 'g--','LineWidth',2);
    plot(P_D_saltos_erros.time, P_D_saltos_erros.signals.values(:,1), 'ro','LineWidth',2);
    plot(P_D_saltos_erros.time, P_D_saltos_erros.signals.values(:,2), 'r+','LineWidth',2);
    legend('Posicao Real x', 'Posicao Real y','Estimativa de Kalman x', 'Estimativa de Kalman y', 'Posi��o Medida x', 'Posi��o Medida y');
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Objecto ao longo do tempo');
    
    % grafico com o erro de kalman
    figure(); hold on; grid on;
    plot(erro_kalman_discreto.time, erro_kalman_discreto.signals.values(:,1), 'b', 'LineWidth',1.5);
    plot(erro_kalman_discreto.time, erro_kalman_discreto.signals.values(:,2), 'g', 'LineWidth',1.5);

    legend('erro x', 'erro y');
    xlabel('Tempo [s]'); ylabel('Erro [m]');
    title('Erro na estimativa da Posi��o do Objecto');
    
    % Tra�o da covari�ncia
    tamanho = size(Covariancia.signals.values, 3);
    traco = zeros(tamanho, 1);
    for i=1:tamanho
        traco(i) = trace(Covariancia.signals.values(:,:,i));
    end
    figure(); hold on; grid on;
    plot(Covariancia.time, traco, 'b');
    xlabel('Tempo [s]'); ylabel('Tra�o da Covari�ncia');
    title('Tra�o da Covari�ncia do Erro de Kalman');
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Kalman hibrido
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if kh
    % no inicio parece ter 2 estimativas em vez de uma, tem 2 tra?os, ver
    % porque parece ser devido ?s condi??es iniciais
    
    disp('*****kalman hibrido*****');
    sim('kalman_hibrido', tempo_simulacao);
    %sim('kalman_hibrido_integrais', tempo_simulacao);

    % gr?fico real vs estimado
    figure(); hold on; grid on;
    plot(P_D.signals.values(:,1), P_D.signals.values(:,2), 'bo','LineWidth',1);
    plot(P_D_Kalman.signals.values(:,1), P_D_Kalman.signals.values(:,2), 'gx','LineWidth',1);
    plot(P_D_Saltos.signals.values(:,1), P_D_Saltos.signals.values(:,2), 'rx','LineWidth',2);

    legend('Posicao Real', 'Estimativa de Kalman', 'Posi��o Medida');
    xlabel('x [m]'); ylabel('y [m]');
    title('Traject�ria do Objecto');
   
    % gr�fico x e y em fun��o do tempo
    figure(); hold on; grid on;
    plot(P_D.time, P_D.signals.values(:,1), 'bo','LineWidth',0.1);   
    plot(P_D.time, P_D.signals.values(:,2), 'b+','LineWidth',1);
    plot(P_D_Kalman.time, P_D_Kalman.signals.values(:,1), 'g','LineWidth',2);
    plot(P_D_Kalman.time, P_D_Kalman.signals.values(:,2), 'g--','LineWidth',2);
    plot(P_D_Saltos.time, P_D_Saltos.signals.values(:,1), 'ro','LineWidth',2);
    plot(P_D_Saltos.time, P_D_Saltos.signals.values(:,2), 'r+','LineWidth',2);
    legend('Posicao Real x', 'Posicao Real y','Estimativa de Kalman x', 'Estimativa de Kalman y', 'Posi��o Medida x', 'Posi��o Medida y');
    xlabel('Tempo [s]'); ylabel('Posi��o [m]');
    title('Traject�ria do Objecto ao longo do tempo');
    
    % gr?fico com o erro de kalman
    figure(); hold on; grid on;
    plot(erro_kalman_discreto.time, erro_kalman_discreto.signals.values(:,1), 'b', 'LineWidth',1.5);
    plot(erro_kalman_discreto.time, erro_kalman_discreto.signals.values(:,2), 'g', 'LineWidth',1.5);

    legend('erro x', 'erro y');
    xlabel('Tempo [s]'); ylabel('Erro [m]');
    title('Erro na estimativa da Posi��o do Objecto');
    
    % Tra�o da covari�ncia
    tamanho = size(Covariancia.signals.values, 3);
    traco = zeros(tamanho, 1);
    for i=1:tamanho
        traco(i) = trace(Covariancia.signals.values(:,:,i));
    end
    figure(); hold on; grid on;
    plot(Covariancia.time, traco, 'b');
    xlabel('Tempo [s]'); ylabel('Tra�o da Covari�ncia');
    title('Tra�o da Covari�ncia do Erro de Kalman');
    
    
end
