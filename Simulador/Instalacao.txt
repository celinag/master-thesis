Instalação

O simulador foi desenvolvido em Matlab, versão 7.9.0 (R2009b) e em Simulink onde foram usadas Embedded Functions, que necessitam de ter instalado no sistema um compilador de C como o GCC.

É necessário adicionar a biblioteca desenvolvida, System Vehicles for Human in Sea, às restantes bibliotecas do Simulink, recorrendo à ferramenta pathtool.